<?php

namespace ITPassionLtd\Laravel\GeoInfo\Model;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	/**
	 * The currencies used in this country
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function currencies()
	{
		return $this->belongsToMany(
			'ITPassionLtd\Laravel\GeoInfo\Model\Currency');
	}

	/**
	 * The languages spoken in this country
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function languages()
	{
		return $this->belongsToMany(
			'ITPassionLtd\Laravel\GeoInfo\Model\Language');
	}

	/**
	 * Attributes that should be shown as dates
	 *
	 * @var array
	 */
	protected $dates = ['created_at', 'updated_at'];
}
