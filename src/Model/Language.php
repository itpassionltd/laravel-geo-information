<?php

namespace ITPassionLtd\Laravel\GeoInfo\Model;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
	/**
	 * The countries that speak this language
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function countries()
	{
		return $this->belongsToMany(
			'ITPassionLtd\Laravel\GeoInfo\Model\Country');
	}

	/**
	 * Attributes that should be shown as dates
	 *
	 * @var array
	 */
	protected $dates = ['created_at', 'updated_at'];
}
