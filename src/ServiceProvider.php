<?php

namespace ITPassionLtd\Laravel\GeoInfo;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
	/**
	 * Bootstrap the application services
	 *
	 * @return void
	 */
	public function boot()
	{
		if($this->app->runningInConsole()) {
			$this->register_migrations();
		}
	}

	/**
	 * Register the service provider
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	/**
	 * Register GeoInfo's migrations
	 *
	 * @return void
	 */
	protected function register_migrations()
	{
		$this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
	}
}