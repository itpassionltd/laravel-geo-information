<?php

namespace ITPassionLtd\Laravel\GeoInfo;

use Illuminate\Support\Facades\Route;

class GeoInformation
{
	/**
	 * Binds the GeoInformation routes into the controller
	 */
	public static function routes($callback = null, array $options = [])
	{
		$callback = $callback ?: function($router) {
			$router->all();
		};

		$default_options = [
			'prefix' => '/geoinfo',
			'namespace' => '\ITPassionLtd\Laravel\GeoInfo\Http\Controllers',
		];
		$options = array_merge($default_options, $options);

		Route::group($options, function($router) use ($callback) {
			$callback(new RouteRegistrar($router));
		});
	}
}
