<?php

namespace ITPassionLtd\Laravel\GeoInfo\Seeds;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountriesTableSeeder extends Seeder
{
	/**
	 * Run the seeder
	 *
	 * @return void
	 */
	public function run()
	{
		/*
		 * TODO Copy countries from
		 * https://www.iso.org/obp/ui/#search
		 */

		DB::table('countries')->insert([
			'short_name_en' => 'Afghanistan',
			'short_name_uppercase_en' => 'AFGHANISTAN',
			'full_name_en' => 'the Islamic Republic of Afghanistan',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'AF',
			'alpha_3_code' => 'AFG',
			'numeric_code' => '004',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Åland Islands',
			'short_name_uppercase_en' => 'ÅLAND ISLANDS',
			'status' => 'officially-assigned',
			'independent' => 0,
			'alpha_2_code' => 'AX',
			'alpha_3_code' => 'ALA',
			'numeric_code' => '248',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Albania',
			'short_name_uppercase_en' => 'ALBANIA',
			'full_name_en' => 'the Republic of Albania',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'AL',
			'alpha_3_code' => 'ALB',
			'numeric_code' => '008',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Algeria',
			'short_name_uppercase_en' => 'ALGERIA',
			'full_name_en' => 'the People\'s Democratic Republic of Algeria',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'DZ',
			'alpha_3_code' => 'DZA',
			'numeric_code' => '012',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'American Samoa',
			'short_name_uppercase_en' => 'AMERICAN SAMOA',
			'status' => 'officially-assigned',
			'independent' => 0,
			'alpha_2_code' => 'AS',
			'alpha_3_code' => 'ASM',
			'numeric_code' => '016',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Andorra',
			'short_name_uppercase_en' => 'ANDORRA',
			'full_name_en' => 'the Principality of Andorra',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'AD',
			'alpha_3_code' => 'AND',
			'numeric_code' => '020',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Angola',
			'short_name_uppercase_en' => 'ANGOLA',
			'full_name_en' => 'the Republic of Angola',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'AO',
			'alpha_3_code' => 'AGO',
			'numeric_code' => '024',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Anguilla',
			'short_name_uppercase_en' => 'ANGUILLA',
			'status' => 'officially-assigned',
			'independent' => 0,
			'alpha_2_code' => 'AI',
			'alpha_3_code' => 'AIA',
			'numeric_code' => '660',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Antarctica',
			'short_name_uppercase_en' => 'ANTARCTICA',
			'status' => 'officially-assigned',
			'independent' => 0,
			'alpha_2_code' => 'AQ',
			'alpha_3_code' => 'ATA',
			'numeric_code' => '010',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Antigua and Barbuda',
			'short_name_uppercase_en' => 'ANTIGUA AND BARBUDA',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'AG',
			'alpha_3_code' => 'ATG',
			'numeric_code' => '028',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Argentina',
			'short_name_uppercase_en' => 'ARGENTINA',
			'full_name_en' => 'the Argentine Republic',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'AR',
			'alpha_3_code' => 'ARG',
			'numeric_code' => '032',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Armenia',
			'short_name_uppercase_en' => 'ARMENIA',
			'full_name_en' => 'the Republic of Armenia',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'AM',
			'alpha_3_code' => 'ARM',
			'numeric_code' => '051',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Aruba',
			'short_name_uppercase_en' => 'ARUBA',
			'status' => 'officially-assigned',
			'independent' => 0,
			'alpha_2_code' => 'AW',
			'alpha_3_code' => 'ABW',
			'numeric_code' => '533',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Australia',
			'short_name_uppercase_en' => 'AUSTRALIA',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'AU',
			'alpha_3_code' => 'AUS',
			'numeric_code' => '036',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Austria',
			'short_name_uppercase_en' => 'AUSTRIA',
			'full_name_en' => 'the Republic of Austria',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'AT',
			'alpha_3_code' => 'AUT',
			'numeric_code' => '040',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Azerbaijan',
			'short_name_uppercase_en' => 'AZERBAIJAN',
			'full_name_en' => 'the Republic of Azerbaijan',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'AZ',
			'alpha_3_code' => 'AZE',
			'numeric_code' => '031',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Bahamas (the)',
			'short_name_uppercase_en' => 'BAHAMAS',
			'full_name_en' => 'the Commonwealth of the Bahamas',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'BS',
			'alpha_3_code' => 'BHS',
			'numeric_code' => '044',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Bahrain',
			'short_name_uppercase_en' => 'BAHRAIN',
			'full_name_en' => 'the Kingdom of Bahrain',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'BH',
			'alpha_3_code' => 'BHR',
			'numeric_code' => '048',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Bangladesh',
			'short_name_uppercase_en' => 'BANGLADESH',
			'full_name_en' => 'the People\'s Republic of Bangladesh',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'BD',
			'alpha_3_code' => 'BGD',
			'numeric_code' => '050',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Barbados',
			'short_name_uppercase_en' => 'BARBADOS',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'BB',
			'alpha_3_code' => 'BRB',
			'numeric_code' => '052',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Belarus',
			'short_name_uppercase_en' => 'BELARUS',
			'full_name_en' => 'the Republic of Belarus',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'BY',
			'alpha_3_code' => 'BLR',
			'numeric_code' => '112',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Belgium',
			'short_name_uppercase_en' => 'BELGIUM',
			'full_name_en' => 'the Kingdom of Belgium',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'BE',
			'alpha_3_code' => 'BEL',
			'numeric_code' => '056',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Belize',
			'short_name_uppercase_en' => 'BELIZE',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'BZ',
			'alpha_3_code' => 'BLZ',
			'numeric_code' => '084',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Benin',
			'short_name_uppercase_en' => 'BENIN',
			'full_name_en' => 'the Republic of Benin',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'BJ',
			'alpha_3_code' => 'BEN',
			'numeric_code' => '204',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Bermuda',
			'short_name_uppercase_en' => 'BERMUDA',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'BM',
			'alpha_3_code' => 'BMU',
			'numeric_code' => '060',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Bhutan',
			'short_name_uppercase_en' => 'BHUTAN',
			'full_name_en' => 'the Kingdom of Bhutan',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'BT',
			'alpha_3_code' => 'BTN',
			'numeric_code' => '064',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Bolivia (Plurinational State of)',
			'short_name_uppercase_en' => 'BOLIVIA (PLURINATIONAL STATE OF)',
			'full_name_en' => 'the Plurinational State of Bolivia',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'BO',
			'alpha_3_code' => 'BOL',
			'numeric_code' => '068',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Bonaire, Sint Eustatius and Saba',
			'short_name_uppercase_en' => 'BONAIRE, SINT EUSTATIUS AND SABA',
			'status' => 'officially-assigned',
			'independent' => 0,
			'alpha_2_code' => 'BQ',
			'alpha_3_code' => 'BES',
			'numeric_code' => '535',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Bosnia and Herzegovina',
			'short_name_uppercase_en' => 'BOSNIA AND HERZEGOVINA',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'BA',
			'alpha_3_code' => 'BIH',
			'numeric_code' => '070',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Botswana',
			'short_name_uppercase_en' => 'BOTSWANA',
			'full_name_en' => 'the Republic of Botswana',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'BW',
			'alpha_3_code' => 'BWA',
			'numeric_code' => '072',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Bouvet Island',
			'short_name_uppercase_en' => 'BOUVET ISLAND',
			'status' => 'officially-assigned',
			'independent' => 0,
			'alpha_2_code' => 'BV',
			'alpha_3_code' => 'BVT',
			'numeric_code' => '074',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Brazil',
			'short_name_uppercase_en' => 'BRAZIL',
			'full_name_en' => 'the Federative Republic of Brazil',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'BR',
			'alpha_3_code' => 'BRA',
			'numeric_code' => '076',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'British Indian Ocean Territory',
			'short_name_uppercase_en' => 'BRITISH INDIAN OCEAN TERRITORY',
			'status' => 'officially-assigned',
			'independent' => 0,
			'alpha_2_code' => 'IO',
			'alpha_3_code' => 'IOT',
			'numeric_code' => '086',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Brunei Darusalam',
			'short_name_uppercase_en' => 'BRUNEI DARUSALAM',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'BN',
			'alpha_3_code' => 'BRN',
			'numeric_code' => '096',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Bulgaria',
			'short_name_uppercase_en' => 'BULGARIA',
			'full_name_en' => 'the Republic of Bulgaria',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'BG',
			'alpha_3_code' => 'BGR',
			'numeric_code' => '100',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Burkina Faso',
			'short_name_uppercase_en' => 'BURKINA FASO',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'BF',
			'alpha_3_code' => 'BFA',
			'numeric_code' => '854',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Burundi',
			'short_name_uppercase_en' => 'BURUNDI',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'BI',
			'alpha_3_code' => 'BDI',
			'numeric_code' => '108',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Cabo Verde',
			'short_name_uppercase_en' => 'CABO VERDE',
			'full_name_en' => 'the Republic of Cabo Verde',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'CV',
			'alpha_3_code' => 'CPV',
			'numeric_code' => '132',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Cambodia',
			'short_name_uppercase_en' => 'CAMBODIA',
			'full_name_en' => 'the Kingdom of Cambodia',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'KH',
			'alpha_3_code' => 'KHM',
			'numeric_code' => '116',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Cameroon',
			'short_name_uppercase_en' => 'CAMEROON',
			'full_name_en' => 'the Republic of Cameroon',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'CM',
			'alpha_3_code' => 'CMR',
			'numeric_code' => '120',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Canada',
			'short_name_uppercase_en' => 'CANADA',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'CA',
			'alpha_3_code' => 'CAN',
			'numeric_code' => '124',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Cayman Islands (the)',
			'short_name_uppercase_en' => 'CAYMAN ISLANDS',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'KY',
			'alpha_3_code' => 'CYM',
			'numeric_code' => '136',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Central African Republic (the)',
			'short_name_uppercase_en' => 'CENTRAL AFRICAN REPUBLIC',
			'full_name_en' => 'the Central African Republic',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'CF',
			'alpha_3_code' => 'CAF',
			'numeric_code' => '140',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Chad',
			'short_name_uppercase_en' => 'CHAD',
			'full_name_en' => 'the Republic of Chad',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'TD',
			'alpha_3_code' => 'TCD',
			'numeric_code' => '148',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Chile',
			'short_name_uppercase_en' => 'CHILE',
			'full_name_en' => 'the Republic of Chile',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'CL',
			'alpha_3_code' => 'CHL',
			'numeric_code' => '152',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'China',
			'short_name_uppercase_en' => 'CHINA',
			'full_name_en' => "the People's Republic of China",
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'CN',
			'alpha_3_code' => 'CHN',
			'numeric_code' => '156',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Christmas Island',
			'short_name_uppercase_en' => 'CHRISTMAS ISLAND',
			'status' => 'officially-assigned',
			'independent' => 0,
			'alpha_2_code' => 'CX',
			'alpha_3_code' => 'CXR',
			'numeric_code' => '162',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Cocos (Keeling) Islands (the)',
			'short_name_uppercase_en' => 'COCOS (KEELING) ISLANDS',
			'status' => 'officially-assigned',
			'independent' => 10,
			'alpha_2_code' => 'CC',
			'alpha_3_code' => 'CCK',
			'numeric_code' => '166',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Colombia',
			'short_name_uppercase_en' => 'COLOMBIA',
			'full_name_en' => 'the Republic of Colombia',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'CO',
			'alpha_3_code' => 'COL',
			'numeric_code' => '170',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Comoros (the)',
			'short_name_uppercase_en' => 'COMOROS',
			'full_name_en' => 'the Union of the Comoros',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'KM',
			'alpha_3_code' => 'COM',
			'numeric_code' => '174',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Congo (the Democratic Republic of the)',
			'short_name_uppercase_en' => 'CONGO, DEMOCRATIC REPUBLIC OF THE',
			'full_name_en' => 'the Democratic Republic of the Congo',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'CD',
			'alpha_3_code' => 'COD',
			'numeric_code' => '180',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Congo (the)',
			'short_name_uppercase_en' => 'CONGO',
			'full_name_en' => 'the Republic of the Congo',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'CG',
			'alpha_3_code' => 'COG',
			'numeric_code' => '178',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Cook Islands (the)',
			'short_name_uppercase_en' => 'COOK ISLANDS',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'CK',
			'alpha_3_code' => 'COK',
			'numeric_code' => '184',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Costa Rica',
			'short_name_uppercase_en' => 'COSTA RICA',
			'full_name_en' => 'the Republic of Costa Rica',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'CR',
			'alpha_3_code' => 'CRI',
			'numeric_code' => '188',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Côte d\'Ivoire',
			'short_name_uppercase_en' => 'CÔTE D\'IVOIRE',
			'full_name_en' => 'the Republic of Côte d\'Ivoire',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'CI',
			'alpha_3_code' => 'CIV',
			'numeric_code' => '384',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Croatia',
			'short_name_uppercase_en' => 'CROATIA',
			'full_name_en' => 'the Republic of Croatia',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'HR',
			'alpha_3_code' => 'HRV',
			'numeric_code' => '191',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Cuba',
			'short_name_uppercase_en' => 'CUBA',
			'full_name_en' => 'the Republic of Cuba',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'CU',
			'alpha_3_code' => 'CUB',
			'numeric_code' => '192',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Curaçao',
			'short_name_uppercase_en' => 'CURAÇAO',
			'status' => 'officially-assigned',
			'independent' => 0,
			'alpha_2_code' => 'CW',
			'alpha_3_code' => 'CUW',
			'numeric_code' => '531',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Cyprus',
			'short_name_uppercase_en' => 'CYPRUS',
			'full_name_en' => 'the Republic of Cyprus',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'CY',
			'alpha_3_code' => 'CYP',
			'numeric_code' => '196',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Czechia',
			'short_name_uppercase_en' => 'CZECHIA',
			'full_name_en' => 'the Czech Republic',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'CZ',
			'alpha_3_code' => 'CZE',
			'numeric_code' => '203',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Denmark',
			'short_name_uppercase_en' => 'DENMARK',
			'full_name_en' => 'the Kingdom of Denmark',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'DK',
			'alpha_3_code' => 'DNK',
			'numeric_code' => '208',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Djibouti',
			'short_name_uppercase_en' => 'DJIBOUTI',
			'full_name_en' => 'the Republic of Djibouti',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'DJ',
			'alpha_3_code' => 'DJI',
			'numeric_code' => '262',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Dominica',
			'short_name_uppercase_en' => 'DOMINICA',
			'full_name_en' => 'the Commonwealth of Dominica',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'DM',
			'alpha_3_code' => 'DMA',
			'numeric_code' => '212',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Dominican Republic (the)',
			'short_name_uppercase_en' => 'DOMINICAN REPUBLIC',
			'full_name_en' => 'the Dominican Republic',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'DO',
			'alpha_3_code' => 'DOM',
			'numeric_code' => '214',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Ecuador',
			'short_name_uppercase_en' => 'ECUADOR',
			'full_name_en' => 'the Republic of Ecuador',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'EC',
			'alpha_3_code' => 'ECU',
			'numeric_code' => '218',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Egypt',
			'short_name_uppercase_en' => 'EGYPT',
			'full_name_en' => 'the Arab Republic of Egypt',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'EG',
			'alpha_3_code' => 'EGY',
			'numeric_code' => '818',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'El Salvador',
			'short_name_uppercase_en' => 'EL SALVADOR',
			'full_name_en' => 'the Republic of El Salvador',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'SV',
			'alpha_3_code' => 'SLV',
			'numeric_code' => '222',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Equatorial Guinea',
			'short_name_uppercase_en' => 'EQUATORIAL GUINEA	',
			'full_name_en' => 'the Republic of Equatorial Guinea',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'GQ',
			'alpha_3_code' => 'GNQ',
			'numeric_code' => '226',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Eritrea',
			'short_name_uppercase_en' => 'ERITREA',
			'full_name_en' => 'the State of Eritrea',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'ER',
			'alpha_3_code' => 'ERI',
			'numeric_code' => '232',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Estonia',
			'short_name_uppercase_en' => 'ESTONIA',
			'full_name_en' => 'the Republic of Estonia',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'EE',
			'alpha_3_code' => 'EST',
			'numeric_code' => '233',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Ethiopia',
			'short_name_uppercase_en' => 'ETHIOPIA',
			'full_name_en' => 'the Federal Democratic Repulic of Ethiopia',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'ET',
			'alpha_3_code' => 'ETH',
			'numeric_code' => '231',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Falkland Islands (the) [Malvinas]',
			'short_name_uppercase_en' => 'FALKLAND ISLANDS (MALVINAS)',
			'status' => 'officially-assigned',
			'independent' => 0,
			'alpha_2_code' => 'FK',
			'alpha_3_code' => 'FLK',
			'numeric_code' => '238',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Faroe Islands (the)',
			'short_name_uppercase_en' => 'FAROE ISLANDS',
			'status' => 'officially-assigned',
			'independent' => 0,
			'alpha_2_code' => 'FO',
			'alpha_3_code' => 'FRO',
			'numeric_code' => '234',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Fiji',
			'short_name_uppercase_en' => 'FIJI',
			'full_name_en' => 'the Republic of Fiji',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'FJ',
			'alpha_3_code' => 'FJI',
			'numeric_code' => '242',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Finland',
			'short_name_uppercase_en' => 'FINLAND',
			'full_name_en' => 'the Republic of Finland',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'FI',
			'alpha_3_code' => 'FIN',
			'numeric_code' => '246',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'France',
			'short_name_uppercase_en' => 'FRANCE',
			'full_name_en' => 'the French Republic',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'FR',
			'alpha_3_code' => 'FRA',
			'numeric_code' => '250',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'French Guiana',
			'short_name_uppercase_en' => 'FRENCH GUIANA',
			'status' => 'officially-assigned',
			'independent' => 0,
			'alpha_2_code' => 'GF',
			'alpha_3_code' => 'GUF',
			'numeric_code' => '254',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'French Polynesia',
			'short_name_uppercase_en' => 'FRENCH POLYNESIA',
			'status' => 'officially-assigned',
			'independent' => 0,
			'alpha_2_code' => 'PF',
			'alpha_3_code' => 'PYF',
			'numeric_code' => '258',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'French Southern Territories (the)',
			'short_name_uppercase_en' => 'FRENCH SOUTHERN TERRITORIES',
			'status' => 'officially-assigned',
			'independent' => 0,
			'alpha_2_code' => 'TF',
			'alpha_3_code' => 'ATF',
			'numeric_code' => '260',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Gabon',
			'short_name_uppercase_en' => 'GABON',
			'full_name_en' => 'the Gabonese Republic',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'GA',
			'alpha_3_code' => 'GAB',
			'numeric_code' => '266',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Gambia (the)',
			'short_name_uppercase_en' => 'GAMBIA',
			'full_name_en' => 'the Republic of the Gambia',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'GM',
			'alpha_3_code' => 'GMB',
			'numeric_code' => '270',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Georgia',
			'short_name_uppercase_en' => 'GEORGIA',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'GE',
			'alpha_3_code' => 'GEO',
			'numeric_code' => '268',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Germany',
			'short_name_uppercase_en' => 'GERMANY',
			'full_name_en' => 'the Federal Republic of Germany',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'DE',
			'alpha_3_code' => 'DEU',
			'numeric_code' => '276',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Ghana',
			'short_name_uppercase_en' => 'GHANA',
			'full_name_en' => 'the Republic of Ghana',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'GH',
			'alpha_3_code' => 'GHA',
			'numeric_code' => '288',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Gibraltar',
			'short_name_uppercase_en' => 'GIBRALTAR',
			'status' => 'officially-assigned',
			'independent' => 0,
			'alpha_2_code' => 'GI',
			'alpha_3_code' => 'GIB',
			'numeric_code' => '292',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Greece',
			'short_name_uppercase_en' => 'GREECE',
			'full_name_en' => 'the Hellenic Republic',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'GR',
			'alpha_3_code' => 'GRV',
			'numeric_code' => '300',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Greenland',
			'short_name_uppercase_en' => 'GREENLAND',
			'status' => 'officially-assigned',
			'independent' => 0,
			'alpha_2_code' => 'GR',
			'alpha_3_code' => 'GRL',
			'numeric_code' => '304',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Grenada',
			'short_name_uppercase_en' => 'GRENADA',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'GD',
			'alpha_3_code' => 'GRD',
			'numeric_code' => '308',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Guadeloupe',
			'short_name_uppercase_en' => 'GUADELOUPE',
			'status' => 'officially-assigned',
			'independent' => 0,
			'alpha_2_code' => 'GP',
			'alpha_3_code' => 'GLP',
			'numeric_code' => '312',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Guam',
			'short_name_uppercase_en' => 'GUAM',
			'status' => 'officially-assigned',
			'independent' => 0,
			'alpha_2_code' => 'GU',
			'alpha_3_code' => 'GUM',
			'numeric_code' => '316',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Guatamala',
			'short_name_uppercase_en' => 'GUATAMALA',
			'full_name_en' => 'the Republic of Guatamala',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'GT',
			'alpha_3_code' => 'GTM',
			'numeric_code' => '320',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Guernsey',
			'short_name_uppercase_en' => 'GUERNSEY',
			'status' => 'officially-assigned',
			'independent' => 0,
			'alpha_2_code' => 'GG',
			'alpha_3_code' => 'GGY',
			'numeric_code' => '831',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Guinea',
			'short_name_uppercase_en' => 'GUINEA',
			'full_name_en' => 'the Republic of Guinea',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'GN',
			'alpha_3_code' => 'GIN',
			'numeric_code' => '324',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Guinea-Bissau',
			'short_name_uppercase_en' => 'GUINEA-BISSAU',
			'full_name_en' => 'the Republic of Guinea-Bissau',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'GW',
			'alpha_3_code' => 'GNB',
			'numeric_code' => '624',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Guyana',
			'short_name_uppercase_en' => 'GUYANA',
			'full_name_en' => 'the Republic of Guyana',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'GY',
			'alpha_3_code' => 'GUY',
			'numeric_code' => '328',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Haiti',
			'short_name_uppercase_en' => 'HAITI',
			'full_name_en' => 'the Republic of Haiti',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'HT',
			'alpha_3_code' => 'HTI',
			'numeric_code' => '332',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Heard Island and McDonald Islands',
			'short_name_uppercase_en' => 'HEARD ISLAND AND MCDONALD ISLANDS',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'HM',
			'alpha_3_code' => 'HMD',
			'numeric_code' => '334',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Holy See (the)',
			'short_name_uppercase_en' => 'HOLY SEE',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'VA',
			'alpha_3_code' => 'VAT',
			'numeric_code' => '336',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Honduras',
			'short_name_uppercase_en' => 'HONDURAS',
			'full_name_en' => 'the Republic of Honduras',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'HN',
			'alpha_3_code' => 'HND',
			'numeric_code' => '340',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Hong Kong',
			'short_name_uppercase_en' => 'HONG KONG',
			'full_name_en' => 'the Hong Kong Special Administrative Region ' .
				'of China',
			'status' => 'officially-assigned',
			'independent' => 0,
			'alpha_2_code' => 'HK',
			'alpha_3_code' => 'HKG',
			'numeric_code' => '344',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Hungary',
			'short_name_uppercase_en' => 'HUNGARY',
			'status' => 'officially-assgined',
			'independent' => 1,
			'alpha_2_code' => 'HU',
			'alpha_3_code' => 'HUN',
			'numeric_code' => '348',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Icenland',
			'short_name_uppercase_en' => 'ICELAND',
			'full_name_en' => 'the Republic of Iceland',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'IS',
			'alpha_3_code' => 'ISL',
			'numeric_code' => '352',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'India',
			'short_name_uppercase_en' => 'INDIA',
			'full_name_en' => 'the Republic of India',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'IN',
			'alpha_3_code' => 'IND',
			'numeric_code' => '356',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Indonesia',
			'short_name_uppercase_en' => 'INDONESIA',
			'full_name_en' => 'the Republic of Indonesia',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'ID',
			'alpha_3_code' => 'IDN',
			'numeric_code' => '360',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Iran (Islamic Republic of)',
			'short_name_uppercase_en' => 'IRAN (ISLAMIC REPUBLIC OF)',
			'full_name_en' => 'the Islamic Republic of Iran',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'IR',
			'alpha_3_code' => 'IRN',
			'numeric_code' => '364',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Iraq',
			'short_name_uppercase_en' => 'IRAQ',
			'full_name_en' => 'the Republic of Iraq',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'IQ',
			'alpha_3_code' => 'IRQ',
			'numeric_code' => '368',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Ireland',
			'short_name_uppercase_en' => 'IRELAND',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'IE',
			'alpha_3_code' => 'IRL',
			'numeric_code' => '372',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Isle of Man',
			'short_name_uppercase_en' => 'ISLE OF MAN',
			'status' => 'officially-assigned',
			'independent' => 0,
			'alpha_2_code' => 'IM',
			'alpha_3_code' => 'IMN',
			'numeric_code' => '833',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Israel',
			'short_name_uppercase_en' => 'ISRAEL',
			'full_name_en' => 'the State of Israel',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'IL',
			'alpha_3_code' => 'ISR',
			'numeric_code' => '376',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Italy',
			'short_name_uppercase_en' => 'ITALY',
			'full_name_en' => 'the Republic of Italy',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'IT',
			'alpha_3_code' => 'ITA',
			'numeric_code' => '380',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Jamaica',
			'short_name_uppercase_en' => 'JAMAICA',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'JM',
			'alpha_3_code' => 'JAM',
			'numeric_code' => '388',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Japan',
			'short_name_uppercase_en' => 'JAPAN',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'JP',
			'alpha_3_code' => 'JPN',
			'numeric_code' => '392',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Jersey',
			'short_name_uppercase_en' => 'JERSEY',
			'status' => 'officially-assigned',
			'independent' => 0,
			'alpha_2_code' => 'JE',
			'alpha_3_code' => 'JEY',
			'numeric_code' => '832',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('countries')->insert([
			'short_name_en' => 'Jordan',
			'short_name_uppercase_en' => 'JORDAN',
			'full_name_en' => 'the Hashemite Kingdom of Jordan',
			'status' => 'officially-assigned',
			'independent' => 1,
			'alpha_2_code' => 'JO',
			'alpha_3_code' => 'JOR',
			'numeric_code' => '400',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
//
//		DB::table('countries')->insert([
//			'short_name_en' => '',
//			'short_name_uppercase_en' => '',
//			'full_name_en' => '',
//			'status' => '',
//			'independent' => 1,
//			'alpha_2_code' => '',
//			'alpha_3_code' => '',
//			'numeric_code' => '',
//			'created_at' => Carbon::now(),
//			'updated_at' => Carbon::now(),
//		]);
	}
}