<?php

namespace ITPassionLtd\Laravel\GeoInfo\Seeds;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use ITPassionLtd\Laravel\GeoInfo\Model\Country;
use ITPassionLtd\Laravel\GeoInfo\Model\Currency;
use ITPassionLtd\Laravel\GeoInfo\Model\Language;

class CountryCurrencyTableSeeder extends Seeder
{
	/**
	 * Run the seeder
	 *
	 * @return void
	 */
	public function run()
	{
		$afghani = Currency::where('alpha_3_code', 'AFN')->first();
		$algerian_dinar = Currency::where('alpha_3_code', 'DZD')
			->first();
		$argentine_peso = Currency::where('alpha_3_code', 'ARS')->first();
		$armenian_dram = Currency::where('alpha_3_code', 'AMD')->first();
		$aruban_florin = Currency::where('alpha_3_code', 'AWG')->first();
		$australian_dollar = Currency::where('alpha_3_code', 'AUD')->first();
		$azerbaijan_manat = Currency::where('alpha_3_code', 'AZN')->first();
		$bahamian_dollar = Currency::where('alpha_3_code', 'BSD')->first();
		$bahraini_dinar = Currency::where('alpha_3_code', 'BHD')->first();
		$barbados_dollar = Currency::where('alpha_3_code', 'BBD')->first();
		$belarusian_ruble = Currency::where('alpha_3_code', 'BYN')->first();
		$belize_dollar = Currency::where('alpha_3_code', 'BZD')->first();
		$bermudian_dollar = Currency::where('alpha_3_code', 'BMD')->first();
		$boliviano = Currency::where('alpha_3_code', 'BOB')->first();
		$brazilian_real = Currency::where('alpha_3_code', 'BRL')->first();
		$brunei_dollar = Currency::where('alpha_3_code', 'BND')->first();
		$bulgarian_lev = Currency::where('alpha_3_code', 'BGN')->first();
		$burundi_franc = Currency::where('alpha_3_code', 'BIF')->first();
		$cabo_verde_escude = Currency::where('alpha_3_code', 'CVE')->first();
		$canadian_dollar = Currency::where('alpha_3_code', 'CAD')->first();
		$cayman_islands_dollar = Currency::where('alpha_3_code', 'KYD')
			->first();
		$cfa_franc_bceao = Currency::where('alpha_3_code', 'XOF')->first();
		$cfa_franc_beac = Currency::where('alpha_3_code', 'XAF')->first();
		$cfp_franc = Currency::where('alpha_3_code', 'XPF')->first();
		$chilean_peso = Currency::where('alpha_3_code', 'CLP')->first();
		$colombian_peso = Currency::where('alpha_3_code', 'COP')->first();
		$comorian_franc = Currency::where('alpha_3_code', 'KMF')->first();
		$congolese_franc = Currency::where('alpha_3_code', 'CDF')->first();
		$convertible_mark = Currency::where('alpha_3_code', 'BAM')->first();
		$costa_rican_colon = Currency::where('alpha_3_code', 'CRC')->first();
		$cuban_peso = Currency::where('alpha_3_code', 'CUP')->first();
		$czech_koruna = Currency::where('alpha_3_code', 'CZK')->first();
		$dalasi = Currency::where('alpha_3_code', 'GMD')->first();
		$danish_krone = Currency::where('alpha_3_code', 'DKK')->first();
		$djibouti_franc = Currency::where('alpha_3_code', 'DJF')->first();
		$dominican_peso = Currency::where('alpha_3_code', 'DOP')->first();
		$east_caribbean_dollar = Currency::where('alpha_3_code', 'XCD')
			->first();
		$egyptian_pound = Currency::where('alpha_3_code', 'EGP')->first();
		$el_salvador_colon = Currency::where('alpha_3_code', 'SVC')->first();
		$ethiopian_birr = Currency::where('alpha_3_code', 'ETB')->first();
		$euro = Currency::where('alpha_3_code', 'EUR')->first();
		$falkland_islands_pound = Currency::where('alpha_3_code', 'FKP')
			->first();
		$fiji_dollar = Currency::where('alpha_3_code', 'FJD')->first();
		$forint = Currency::where('alpha_3_code', 'HUF')->first();
		$ghana_cedi = Currency::where('alpha_3_code', 'GHS')->first();
		$gibraltar_pound = Currency::where('alpha_3_code', 'GIP')->first();
		$gourde = Currency::where('alpha_3_code', 'HTG')->first();
		$guinean_franc = Currency::where('alpha_3_code', 'GNF')->first();
		$guyana_dollar = Currency::where('alpha_3_code', 'GYD')->first();
		$hong_kong_dollar = Currency::where('alpha_3_code', 'HKD')->first();
		$iceland_krona = Currency::where('alpha_3_code', 'ISK')->first();
		$indian_rupee = Currency::where('alpha_3_code', 'INR')->first();
		$iranian_rial = Currency::where('alpha_3_code', 'IRR')->first();
		$iraqi_dinar = Currency::where('alpha_3_code', 'IQD')->first();
		$jamaican_dollar = Currency::where('alpha_3_code', 'JMD')->first();
		$jordanian_dinar = Currency::where('alpha_3_code', 'JOD')->first();
		$kuna = Currency::where('alpha_3_code', 'HRK')->first();
		$kwanza = Currency::where('alpha_3_code', 'AOA')->first();
		$lari = Currency::where('alpha_3_code', 'GEL')->first();
		$lek = Currency::where('alpha_3_code', 'ALL')->first();
		$lempira = Currency::where('alpha_3_code', 'HNL')->first();
		$mvdol = Currency::where('alpha_3_code', 'BOV')->first();
		$nakfa = Currency::where('alpha_3_code', 'ERN')->first();
		$netherlands_antillean_guilder = Currency::where('alpha_3_code', 'ANG')
			->first();
		$new_israeli_sheqel = Currency::where('alpha_3_code', 'ILS')->first();
		$new_zealand_dollar = Currency::where('alpha_3_code', 'NZD')->first();
		$ngultrum = Currency::where('alpha_3_code', 'BTN')->first();
		$norwegian_krone = Currency::where('alpha_3_code', 'NOK')->first();
		$peso_convertible = Currency::where('alpha_3_code', 'CUC')->first();
		$pound_sterling = Currency::where('alpha_3_code', 'GBP')->first();
		$pula = Currency::where('alpha_3_code', 'BWP')->first();
		$quetzal = Currency::where('alpha_3_code', 'GTQ')->first();
		$riel = Currency::where('alpha_3_code', 'KHR')->first();
		$rupiah = Currency::where('alpha_3_code', 'IDR')->first();
		$taka = Currency::where('alpha_3_code', 'BDT')->first();
		$unidad_de_fomento = Currency::where('alpha_3_code', 'CLF')->first();
		$unidad_de_valor_real = Currency::where('alpha_3_code', 'COU')->first();
		$us_dollar = Currency::where('alpha_3_code', 'USD')->first();
		$yen = Currency::where('alpha_3_code', 'JPY')->first();
		$yuan_renminbin = Currency::where('alpha_3_code', 'CNY')->first();

		$country = Country::where('alpha_2_code', 'AD')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $euro->id,
		]);

		$country = Country::where('alpha_2_code', 'AF')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $afghani->id,
		]);

		$country = Country::where('alpha_2_code', 'AG')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $east_caribbean_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'AI')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $east_caribbean_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'AL')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $lek->id,
		]);

		$country = Country::where('alpha_2_code', 'AM')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $armenian_dram->id,
		]);

		$country = Country::where('alpha_2_code', 'AO')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $kwanza->id,
		]);

		$country = Country::where('alpha_2_code', 'AR')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $argentine_peso->id,
		]);

		$country = Country::where('alpha_2_code', 'AS')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $us_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'AT')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $euro->id,
		]);

		$country = Country::where('alpha_2_code', 'AU')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $australian_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'AW')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $aruban_florin->id,
		]);

		$country = Country::where('alpha_2_code', 'AX')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $euro->id,
		]);

		$country = Country::where('alpha_2_code', 'AZ')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $azerbaijan_manat->id,
		]);

		$country = Country::where('alpha_2_code', 'BA')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $convertible_mark->id,
		]);

		$country = Country::where('alpha_2_code', 'BB')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $barbados_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'BD')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $taka->id,
		]);

		$country = Country::where('alpha_2_code', 'BE')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $euro->id,
		]);

		$country = Country::where('alpha_2_code', 'BF')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $cfa_franc_bceao->id,
		]);

		$country = Country::where('alpha_2_code', 'BG')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $bulgarian_lev->id,
		]);

		$country = Country::where('alpha_2_code', 'BH')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $bahraini_dinar->id,
		]);

		$country = Country::where('alpha_2_code', 'BI')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $burundi_franc->id,
		]);

		$country = Country::where('alpha_2_code', 'BJ')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $cfa_franc_bceao->id,
		]);

		$country = Country::where('alpha_2_code', 'BM')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $bermudian_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'BN')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $brunei_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'BO')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $boliviano->id,
		]);
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $mvdol->id,
		]);

		$country = Country::where('alpha_2_code', 'BQ')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $us_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'BR')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $brazilian_real->id,
		]);

		$country = Country::where('alpha_2_code', 'BS')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $bahamian_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'BT')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $indian_rupee->id,
		]);
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $ngultrum->id,
		]);

		$country = Country::where('alpha_2_code', 'BV')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $norwegian_krone->id,
		]);

		$country = Country::where('alpha_2_code', 'BW')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $pula->id,
		]);

		$country = Country::where('alpha_2_code', 'BY')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $belarusian_ruble->id,
		]);

		$country = Country::where('alpha_2_code', 'BZ')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $belize_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'CA')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $canadian_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'CC')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $australian_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'CD')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $congolese_franc->id,
		]);

		$country = Country::where('alpha_2_code', 'CF')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $cfa_franc_beac->id,
		]);

		$country = Country::where('alpha_2_code', 'CG')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $cfa_franc_beac->id,
		]);

		$country = Country::where('alpha_2_code', 'CI')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $cfa_franc_bceao->id,
		]);

		$country = Country::where('alpha_2_code', 'CK')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $new_zealand_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'CL')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $chilean_peso->id,
		]);
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $unidad_de_fomento->id,
		]);

		$country = Country::where('alpha_2_code', 'CM')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $cfa_franc_beac->id,
		]);

		$country = Country::where('alpha_2_code', 'CN')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $yuan_renminbin->id,
		]);

		$country = Country::where('alpha_2_code', 'CO')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $colombian_peso->id,
		]);
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $unidad_de_valor_real->id,
		]);

		$country = Country::where('alpha_2_code', 'CR')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $costa_rican_colon->id,
		]);

		$country = Country::where('alpha_2_code', 'CU')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $cuban_peso->id,
		]);
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $peso_convertible->id,
		]);

		$country = Country::where('alpha_2_code', 'CV')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $cabo_verde_escude->id,
		]);

		$country = Country::where('alpha_2_code', 'CW')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $netherlands_antillean_guilder->id,
		]);

		$country = Country::where('alpha_2_code', 'CX')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $australian_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'CY')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $euro->id,
		]);

		$country = Country::where('alpha_2_code', 'CZ')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $czech_koruna->id,
		]);

		$country = Country::where('alpha_2_code', 'DE')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $euro->id,
		]);

		$country = Country::where('alpha_2_code', 'DJ')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $djibouti_franc->id,
		]);

		$country = Country::where('alpha_2_code', 'DK')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $danish_krone->id,
		]);

		$country = Country::where('alpha_2_code', 'DM')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $east_caribbean_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'DO')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $dominican_peso->id,
		]);

		$country = Country::where('alpha_2_code', 'DZ')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $algerian_dinar->id,
		]);

		$country = Country::where('alpha_2_code', 'EC')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $us_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'EE')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $euro->id,
		]);

		$country = Country::where('alpha_2_code', 'EG')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $egyptian_pound->id,
		]);

		$country = Country::where('alpha_2_code', 'ER')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $nakfa->id,
		]);

		$country = Country::where('alpha_2_code', 'ET')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $ethiopian_birr->id,
		]);

		$country = Country::where('alpha_2_code', 'FI')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $euro->id,
		]);

		$country = Country::where('alpha_2_code', 'FJ')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $fiji_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'FK')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $falkland_islands_pound->id,
		]);

		$country = Country::where('alpha_2_code', 'FO')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $danish_krone->id,
		]);

		$country = Country::where('alpha_2_code', 'FR')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $euro->id,
		]);

		$country = Country::where('alpha_2_code', 'GA')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $cfa_franc_beac->id,
		]);

		$country = Country::where('alpha_2_code', 'GD')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $east_caribbean_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'GE')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $lari->id,
		]);

		$country = Country::where('alpha_2_code', 'GF')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $euro->id,
		]);

		$country = Country::where('alpha_2_code', 'GG')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $pound_sterling->id,
		]);

		$country = Country::where('alpha_2_code', 'GH')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $ghana_cedi->id,
		]);

		$country = Country::where('alpha_2_code', 'GI')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $gibraltar_pound->id,
		]);

		$country = Country::where('alpha_2_code', 'GL')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $danish_krone->id,
		]);

		$country = Country::where('alpha_2_code', 'GM')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $dalasi->id,
		]);

		$country = Country::where('alpha_2_code', 'GN')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $guinean_franc->id,
		]);

		$country = Country::where('alpha_2_code', 'GP')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $euro->id,
		]);

		$country = Country::where('alpha_2_code', 'GQ')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $cfa_franc_beac->id,
		]);

		$country = Country::where('alpha_2_code', 'GR')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $euro->id,
		]);

		$country = Country::where('alpha_2_code', 'GT')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $quetzal->id,
		]);

		$country = Country::where('alpha_2_code', 'GU')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $us_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'GW')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $cfa_franc_bceao->id,
		]);

		$country = Country::where('alpha_2_code', 'GY')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $guyana_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'HK')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $hong_kong_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'HM')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $australian_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'HN')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $lempira->id,
		]);

		$country = Country::where('alpha_2_code', 'HR')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $kuna->id,
		]);

		$country = Country::where('alpha_2_code', 'HT')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $gourde->id,
		]);
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $us_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'HU')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $forint->id,
		]);

		$country = Country::where('alpha_2_code', 'ID')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $rupiah->id,
		]);

		$country = Country::where('alpha_2_code', 'IE')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $euro->id,
		]);

		$country = Country::where('alpha_2_code', 'IM')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $pound_sterling->id,
		]);

		$country = Country::where('alpha_2_code', 'IN')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $indian_rupee->id,
		]);

		$country = Country::where('alpha_2_code', 'IO')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $us_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'IQ')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $iraqi_dinar->id,
		]);

		$country = Country::where('alpha_2_code', 'IR')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $iranian_rial->id,
		]);

		$country = Country::where('alpha_2_code', 'IS')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $iceland_krona->id,
		]);

		$country = Country::where('alpha_2_code', 'IT')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $euro->id,
		]);

		$country = Country::where('alpha_2_code', 'JE')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $pound_sterling->id,
		]);

		$country = Country::where('alpha_2_code', 'JM')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $jamaican_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'JO')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $jordanian_dinar->id,
		]);

		$country = Country::where('alpha_2_code', 'JP')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $yen->id,
		]);

		$country = Country::where('alpha_2_code', 'KH')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $riel->id,
		]);

		$country = Country::where('alpha_2_code', 'KM')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $comorian_franc->id,
		]);

		$country = Country::where('alpha_2_code', 'KY')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $cayman_islands_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'PF')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $cfp_franc->id,
		]);

		$country = Country::where('alpha_2_code', 'SV')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $el_salvador_colon->id,
		]);
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $us_dollar->id,
		]);

		$country = Country::where('alpha_2_code', 'TD')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $cfa_franc_beac->id,
		]);

		$country = Country::where('alpha_2_code', 'TF')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $euro->id,
		]);

		$country = Country::where('alpha_2_code', 'VA')->first();
		DB::table('country_currency')->insert([
			'country_id' => $country->id,
			'currency_id' => $euro->id,
		]);
	}
}