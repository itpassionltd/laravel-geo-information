<?php

namespace ITPassionLtd\Laravel\GeoInfo\Seeds;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LanguagesTableSeeder extends Seeder
{
	/**
	 * Run the seeder
	 *
	 * @return void
	 */
	public function run()
	{
		/*
		 * TODO Copy languages from
		 * https://www.loc.gov/standards/iso639-2/php/code_list.php
		 */

		DB::table('languages')->insert([
			'alpha_3_code' => 'amh',
			'alpha_2_code' => 'am',
			'short_name_en' => 'Amharic',
			'short_name_fr' => 'amharique',
			'short_name_de' => 'Amharisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'ara',
			'alpha_2_code' => 'ar',
			'short_name_en' => 'Arabic',
			'short_name_fr' => 'arabe',
			'short_name_de' => 'Arabisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'aze',
			'alpha_2_code' => 'az',
			'short_name_en' => 'Azerbaijani',
			'short_name_fr' => 'azéri',
			'short_name_de' => 'Aserbeidschanisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'bel',
			'alpha_2_code' => 'be',
			'short_name_en' => 'Belarusian',
			'short_name_fr' => 'biélorusse',
			'short_name_de' => 'Weißrussisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'ben',
			'alpha_2_code' => 'bn',
			'short_name_en' => 'Bengali',
			'short_name_fr' => 'bengali',
			'short_name_de' => 'Bengali',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'bos',
			'alpha_2_code' => 'bs',
			'short_name_en' => 'Bosnian',
			'short_name_fr' => 'bosniaque',
			'short_name_de' => 'Bosnisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'bul',
			'alpha_2_code' => 'bg',
			'short_name_en' => 'Bulgarian',
			'short_name_fr' => 'bulgare',
			'short_name_de' => 'Bulgarisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'cat',
			'alpha_2_code' => 'ca',
			'short_name_en' => 'Catalan;Valencian',
			'short_name_fr' => 'catalan;valencien',
			'short_name_de' => 'Katalanisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'ces',
			'alpha_3_code_synonym' => 'cze',
			'alpha_2_code' => 'cs',
			'short_name_en' => 'Czech',
			'short_name_fr' => 'tchèque',
			'short_name_de' => 'Tschechisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'dan',
			'alpha_2_code' => 'da',
			'short_name_en' => 'Danish',
			'short_name_fr' => 'danois',
			'short_name_de' => 'Dänisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'deu',
			'alpha_3_code_synonym' => 'ger',
			'alpha_2_code' => 'de',
			'short_name_en' => 'German',
			'short_name_fr' => 'allemand',
			'short_name_de' => 'Deutsch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'dut',
			'alpha_3_code_synonym' => 'nld',
			'alpha_2_code' => 'nl',
			'short_name_en' => 'Dutch;Flemish',
			'short_name_fr' => 'néerlandais;flamand',
			'short_name_de' => 'Niederländisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'dzo',
			'alpha_2_code' => 'dz',
			'short_name_en' => 'Dzongkha',
			'short_name_fr' => 'dzongkha',
			'short_name_de' => 'Dzongkha',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'ell',
			'alpha_3_code_synonym' => 'gre',
			'alpha_2_code' => 'el',
			'short_name_en' => 'Greek, Modern (1453 - )',
			'short_name_fr' => 'grec moderne (après 1453)',
			'short_name_de' => 'Neugriechisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'eng',
			'alpha_2_code' => 'en',
			'short_name_en' => 'English',
			'short_name_fr' => 'anglais',
			'short_name_de' => 'Englisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'est',
			'alpha_2_code' => 'et',
			'short_name_en' => 'Estonian',
			'short_name_fr' => 'estonien',
			'short_name_de' => 'Estnisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'fao',
			'alpha_2_code' => 'fo',
			'short_name_en' => 'Faroese',
			'short_name_fr' => 'féroïen',
			'short_name_de' => 'Färöisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'fas',
			'alpha_3_code_synonym' => 'per',
			'alpha_2_code' => 'fa',
			'short_name_en' => 'Persian',
			'short_name_fr' => 'persan',
			'short_name_de' => 'Persisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'fij',
			'alpha_2_code' => 'fj',
			'short_name_en' => 'Fijian',
			'short_name_fr' => 'fidjien',
			'short_name_de' => 'Fidschi-Sprache',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'fin',
			'alpha_2_code' => 'fi',
			'short_name_en' => 'Finnish',
			'short_name_fr' => 'finnois',
			'short_name_de' => 'Finnisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'fra',
			'alpha_3_code_synonym' => 'fre',
			'alpha_2_code' => 'fr',
			'short_name_en' => 'French',
			'short_name_fr' => 'français',
			'short_name_de' => 'Französisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'hat',
			'alpha_2_code' => 'ht',
			'short_name_en' => 'Haitian;Haitian Creole',
			'short_name_fr' => 'haïtien;créole haïtien',
			'short_name_de' => 'Haïtien;Haiti-Kreolisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'heb',
			'alpha_2_code' => 'he',
			'short_name_en' => 'Hebrew',
			'short_name_fr' => 'hébreu',
			'short_name_de' => 'Hebräisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'hin',
			'alpha_2_code' => 'hi',
			'short_name_en' => 'Hindi',
			'short_name_fr' => 'hindi',
			'short_name_de' => 'Hindi',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'hrv',
			'alpha_2_code' => 'hr',
			'short_name_en' => 'Croatian',
			'short_name_fr' => 'croate',
			'short_name_de' => 'Kroatisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'hun',
			'alpha_2_code' => 'hu',
			'short_name_en' => 'Hungarian',
			'short_name_fr' => 'hongrois',
			'short_name_de' => 'Ungarisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'hye',
			'alpha_3_code_synonym' => 'arm',
			'alpha_2_code' => 'hy',
			'short_name_en' => 'Armenian',
			'short_name_fr' => 'arménien',
			'short_name_de' => 'Armenisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'ind',
			'alpha_2_code' => 'id',
			'short_name_en' => 'Indonesian',
			'short_name_fr' => 'indonésien',
			'short_name_de' => 'Bahasa Indonesia',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'isl',
			'alpha_3_code_synonym' => 'ice',
			'alpha_2_code' => 'is',
			'short_name_en' => 'Icelandic',
			'short_name_fr' => 'islandais',
			'short_name_de' => 'Isländisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'ita',
			'alpha_2_code' => 'it',
			'short_name_en' => 'Italian',
			'short_name_fr' => 'italien',
			'short_name_de' => 'Italienisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'jpn',
			'alpha_2_code' => 'ja',
			'short_name_en' => 'Japanese',
			'short_name_fr' => 'japonais',
			'short_name_de' => 'Japanisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'kal',
			'alpha_2_code' => 'kl',
			'short_name_en' => 'Kalaallisut;Greenlandic',
			'short_name_fr' => 'groenlandais',
			'short_name_de' => 'Grönländisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'kat',
			'alpha_3_code_synonym' => 'geo',
			'alpha_2_code' => 'ka',
			'short_name_en' => 'Georgian',
			'short_name_fr' => 'géorgien',
			'short_name_de' => 'Georgisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'khm',
			'alpha_2_code' => 'km',
			'short_name_en' => 'Central Khmer',
			'short_name_fr' => 'khmer central',
			'short_name_de' => 'Kambodschanisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'kur',
			'alpha_2_code' => 'ku',
			'short_name_en' => 'Kurdish',
			'short_name_fr' => 'kurde',
			'short_name_de' => 'Kurdisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'lat',
			'alpha_2_code' => 'la',
			'short_name_en' => 'Latin',
			'short_name_fr' => 'latin',
			'short_name_de' => 'Latein',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'msa',
			'alpha_3_code_synonym' => 'may',
			'alpha_2_code' => 'ms',
			'short_name_en' => 'Malay',
			'short_name_fr' => 'malais',
			'short_name_de' => 'Malaiisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'nno',
			'alpha_2_code' => 'nn',
			'short_name_en' => 'Norwegian Nynorsk;Nynorsk, Norwegian',
			'short_name_fr' => 'norvégien nynorsk;nynorsk, norvégien',
			'short_name_de' => 'Nynorsk',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'nob',
			'alpha_2_code' => 'nb',
			'short_name_en' => 'Bokmål, Norwegian;Norwegian Bokmål',
			'short_name_fr' => 'norvégien bokmål',
			'short_name_de' => 'Bokmål',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'pap',
			'short_name_en' => 'Papiamento',
			'short_name_fr' => 'papiamento',
			'short_name_de' => 'Papiamento',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'por',
			'alpha_2_code' => 'pt',
			'short_name_en' => 'Portuguese',
			'short_name_fr' => 'portugais',
			'short_name_de' => 'Portugiesisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'pus',
			'alpha_2_code' => 'ps',
			'short_name_en' => 'Pushto;Pashto',
			'short_name_fr' => 'pachto',
			'short_name_de' => 'Paschtu',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'spa',
			'alpha_2_code' => 'es',
			'short_name_en' => 'Spanish;Castilian',
			'short_name_fr' => 'espagnol;castillan',
			'short_name_de' => 'Spanisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'run',
			'alpha_2_code' => 'rn',
			'short_name_en' => 'Rundi',
			'short_name_fr' => 'rundi',
			'short_name_de' => 'Rundi-Sprache',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'rus',
			'alpha_2_code' => 'ru',
			'short_name_en' => 'Russian',
			'short_name_fr' => 'russe',
			'short_name_de' => 'Russisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'sag',
			'alpha_2_code' => 'sg',
			'short_name_en' => 'Sango',
			'short_name_fr' => 'sango',
			'short_name_de' => 'Sango-Sprache',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'srp',
			'alpha_2_code' => 'sr',
			'short_name_en' => 'Serbian',
			'short_name_fr' => 'serbe',
			'short_name_de' => 'Serbisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'sqi',
			'alpha_3_code_synonym' => 'alb',
			'alpha_2_code' => 'sq',
			'short_name_en' => 'Albanian',
			'short_name_fr' => 'albanais',
			'short_name_de' => 'Albanisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'swe',
			'alpha_2_code' => 'sv',
			'short_name_en' => 'Swedish',
			'short_name_fr' => 'suédois',
			'short_name_de' => 'Schwedisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'tir',
			'alpha_2_code' => 'ti',
			'short_name_en' => 'Tigrinya',
			'short_name_fr' => 'tigrigna',
			'short_name_de' => 'Tigrinja-Sprache',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'tur',
			'alpha_2_code' => 'tr',
			'short_name_en' => 'Turkish',
			'short_name_fr' => 'turc',
			'short_name_de' => 'Türkisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('languages')->insert([
			'alpha_3_code' => 'zho',
			'alpha_3_code_synonym' => 'chi',
			'alpha_2_code' => 'zh',
			'short_name_en' => 'Chinese',
			'short_name_fr' => 'chinois',
			'short_name_de' => 'Chinesisch',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
//
//		DB::table('languages')->insert([
//			'alpha_3_code' => '',
//			'alpha_3_code_synonym' => '',
//			'alpha_2_code' => '',
//			'short_name_en' => '',
//			'short_name_fr' => '',
//			'short_name_de' => '',
//			'created_at' => Carbon::now(),
//			'updated_at' => Carbon::now(),
//		]);
	}
}
