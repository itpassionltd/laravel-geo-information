<?php

namespace ITPassionLtd\Laravel\GeoInfo\Seeds;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CurrenciesTableSeeder extends Seeder
{
	/**
	 * Run the seeder
	 *
	 * @return void
	 */
	public function run()
	{
		/*
		 * TODO Copy currencies from
		 * https://www.toptal.com/designers/htmlarrows/currency/
		 * and
		 * https://www.currency-iso.org/dam/downloads/lists/list_one.xls
		 */

		DB::table('currencies')->insert([
			'short_name_en' => 'Afghani',
			'alpha_3_code' => 'AFN',
			'numeric_code' => '971',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Algerian Dinar',
			'alpha_3_code' => 'DZD',
			'numeric_code' => '012',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Argentine Peso',
			'alpha_3_code' => 'ARS',
			'numeric_code' => '032',
			'unicode' => 'U+020B1',
			'hex_code' => '&#x20B1',
			'html_entity' => '&#8369;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Armenian Dram',
			'alpha_3_code' => 'AMD',
			'numeric_code' => '051',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Aruban Florin',
			'alpha_3_code' => 'AWG',
			'numeric_code' => '533',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Australian Dollar',
			'alpha_3_code' => 'AUD',
			'numeric_code' => '036',
			'unicode' => 'U+00024',
			'hex_code' => '&#x24;',
			'html_entity' => '&dollar;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Azerbaijan Manat',
			'alpha_3_code' => 'AZN',
			'numeric_code' => '944',
			'unicode' => 'U+020BC',
			'hex_code' => '&#x20BC;',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Bahamian Dollar',
			'alpha_3_code' => 'BSD',
			'numeric_code' => '044',
			'unicode' => 'U+00024',
			'hex_code' => '&#x24;',
			'html_entity' => '&dollar;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Bahraini Dinar',
			'alpha_3_code' => 'BHD',
			'numeric_code' => '048',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 3,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Barbados Dollar',
			'alpha_3_code' => 'BBD',
			'numeric_code' => '052',
			'unicode' => 'U+00024',
			'hex_code' => '&#x24;',
			'html_entity' => '&dollar;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Belarusian Ruble',
			'alpha_3_code' => 'BYN',
			'numeric_code' => '933',
			'unicode' => 'U+020BD',
			'hex_code' => '&#x20BD;',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Belize Dollar',
			'alpha_3_code' => 'BZD',
			'numeric_code' => '084',
			'unicode' => 'U+00024',
			'hex_code' => '&#x24;',
			'html_entity' => '&dollar;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Bermudian Dollar',
			'alpha_3_code' => 'BMD',
			'numeric_code' => '060',
			'unicode' => 'U+00024',
			'hex_code' => '&#x24;',
			'html_entity' => '&dollar;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Boliviano',
			'alpha_3_code' => 'BOB',
			'numeric_code' => '068',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Brazilian Real',
			'alpha_3_code' => 'BRL',
			'numeric_code' => '986',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Brunei Dollar',
			'alpha_3_code' => 'BND',
			'numeric_code' => '096',
			'unicode' => 'U+00024',
			'hex_code' => '&#x24;',
			'html_entity' => '&dollar;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Bulgarian Lev',
			'alpha_3_code' => 'BGN',
			'numeric_code' => '975',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Burundi Franc',
			'alpha_3_code' => 'BIF',
			'numeric_code' => '108',
			'unicode' => 'U+020A3',
			'hex_code' => '&#x20A3;',
			'html_entity' => '',
			'minor_units' => 0,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Cabo Verde Escudo',
			'alpha_3_code' => 'CVE',
			'numeric_code' => '132',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Canadian Dollar',
			'alpha_3_code' => 'CAD',
			'numeric_code' => '124',
			'unicode' => 'U+00024',
			'hex_code' => '&#x24;',
			'html_entity' => '&dollar;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Cayman Islands Dollar',
			'alpha_3_code' => 'KYD',
			'numeric_code' => '136',
			'unicode' => 'U+00024',
			'hex_code' => '&#x24',
			'html_entity' => '&dollar;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'CFA Franc BCEAO',
			'alpha_3_code' => 'XOF',
			'numeric_code' => '952',
			'unicode' => 'U+020A3',
			'hex_code' => '&#x20A3;',
			'html_entity' => '',
			'minor_units' => 0,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'CFA Franc BEAC',
			'alpha_3_code' => 'XAF',
			'numeric_code' => '950',
			'unicode' => 'U+020A3',
			'hex_code' => '&#x20A3;',
			'html_entity' => '',
			'minor_units' => 0,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'CFP Franc',
			'alpha_3_code' => 'XPF',
			'numeric_code' => '953',
			'unicode' => 'U+020A3',
			'hex_code' => '&#x20A3;',
			'html_entity' => '',
			'minor_units' => 0,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Chilean Peso',
			'alpha_3_code' => 'CLP',
			'numeric_code' => '152',
			'unicode' => 'U+020B1',
			'hex_code' => '&#x20B1;',
			'html_entity' => '',
			'minor_units' => 0,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Colombian Peso',
			'alpha_3_code' => 'COP',
			'numeric_code' => '170',
			'unicode' => 'U+020B1',
			'hex_code' => '&#x20B1;',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Comorian Franc',
			'alpha_3_code' => 'KMF',
			'numeric_code' => '174',
			'unicode' => 'U+020A3',
			'hex_code' => '&#x20A3;',
			'html_entity' => '',
			'minor_units' => 0,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Congolese Franc',
			'alpha_3_code' => 'CDF',
			'numeric_code' => '976',
			'unicode' => 'U+020A3',
			'hex_code' => '&#x20A3;',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Convertible Mark',
			'alpha_3_code' => 'BAM',
			'numeric_code' => '977',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Costa Rican Colon',
			'alpha_3_code' => 'CRC',
			'numeric_code' => '188',
			'unicode' => 'U+020A1',
			'hex_code' => '&#x20A1;',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Cuban Peso',
			'alpha_3_code' => 'CUP',
			'numeric_code' => '192',
			'unicode' => 'U+020B1',
			'hex_code' => '&#x20B1',
			'html_entity' => '&#8369;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Czech Koruna',
			'alpha_3_code' => 'CZK',
			'numeric_code' => '203',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Dalasi',
			'alpha_3_code' => 'GMD',
			'numeric_code' => '270',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Danish Krone',
			'alpha_3_code' => 'DKK',
			'numeric_code' => '208',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Djibouti Franc',
			'alpha_3_code' => 'DJF',
			'numeric_code' => '262',
			'unicode' => 'U+020A3',
			'hex_code' => '&#x20A3;',
			'html_entity' => '',
			'minor_units' => 0,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Dominican Peso',
			'alpha_3_code' => 'DOP',
			'numeric_code' => '214',
			'unicode' => 'U+020B1',
			'hex_code' => '&#x20B1',
			'html_entity' => '&#8369;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'East Caribbean Dollar',
			'alpha_3_code' => 'XCD',
			'numeric_code' => '951',
			'unicode' => 'U+00024',
			'hex_code' => '&#x24;',
			'html_entity' => '&dollar;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Egyptian Pound',
			'alpha_3_code' => 'EGP',
			'numeric_code' => '818',
			'unicode' => 'U+000A3',
			'hex_code' => '&#xa3;',
			'html_entity' => '&pound;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'El Salvador Colon',
			'alpha_3_code' => 'SVC',
			'numeric_code' => '222',
			'unicode' => 'U+020A1',
			'hex_code' => '&#x20A1;',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Euro',
			'alpha_3_code' => 'EUR',
			'numeric_code' => '978',
			'unicode' => 'U+020AC',
			'hex_code' => '&#x20AC;',
			'html_entity' => '&euro;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Ehtiopian Birr',
			'alpha_3_code' => 'ETB',
			'numeric_code' => '230',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Falkland Islands Pound',
			'alpha_3_code' => 'FKP',
			'numeric_code' => '238',
			'unicode' => 'U+000A3',
			'hex_code' => '&#xa3;',
			'html_entity' => '&pound;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Fiji Dollar',
			'alpha_3_code' => 'FJD',
			'numeric_code' => '242',
			'unicode' => 'U+00024',
			'hex_code' => '&#x24;',
			'html_entity' => '&dollar;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Forint',
			'alpha_3_code' => 'HUF',
			'numeric_code' => '348',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Ghana Cedi',
			'alpha_3_code' => 'GHS',
			'numeric_code' => '936',
			'unicode' => 'U+020B5',
			'hex_code' => '&#x20B5;',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Gibraltar Pound',
			'alpha_3_code' => 'GIP',
			'numeric_code' => '292',
			'unicode' => 'U+000A3',
			'hex_code' => '&#xa3;',
			'html_entity' => '&pound;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Gourde',
			'alpha_3_code' => 'HTG',
			'numeric_code' => '332',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Guinean Franc',
			'alpha_3_code' => 'GNF',
			'numeric_code' => '324',
			'unicode' => 'U+020A3',
			'hex_code' => '&#x20A3;',
			'html_entity' => '',
			'minor_units' => 0,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Guyana Dollar',
			'alpha_3_code' => 'GYD',
			'numeric_code' => '328',
			'unicode' => 'U+00024',
			'hex_code' => '&#x24;',
			'html_entity' => '&dollar;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Hong Kong Dollar',
			'alpha_3_code' => 'HKD',
			'numeric_code' => '344',
			'unicode' => 'U+00024',
			'hex_code' => '&#x24;',
			'html_entity' => '&dollar;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Iceland Krona',
			'alpha_3_code' => 'ISK',
			'numeric_code' => '352',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 0,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Indian Rupee',
			'alpha_3_code' => 'INR',
			'numeric_code' => '356',
			'unicode' => 'U+020B9',
			'hex_code' => '&#x20B9;',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Iranian Rial',
			'alpha_3_code' => 'IRR',
			'numeric_code' => '364',
			'unicode' => 'U+0FDFC',
			'hex_code' => '&#xFDFC;',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Iraqi Dinar',
			'alpha_3_code' => 'IQD',
			'numeric_code' => '368',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 3,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Jamaican Dollar',
			'alpha_3_code' => 'JMD',
			'numeric_code' => '388',
			'unicode' => 'U+00024',
			'hex_code' => '&#x24;',
			'html_entity' => '&dollar;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Jordanian Dinar',
			'alpha_3_code' => 'JOD',
			'numeric_code' => '400',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 3,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Kuna',
			'alpha_3_code' => 'HRK',
			'numeric_code' => '191',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Kwanza',
			'alpha_3_code' => 'AOA',
			'numeric_code' => '973',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Lari',
			'alpha_3_code' => 'GEL',
			'numeric_code' => '981',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Lek',
			'alpha_3_code' => 'ALL',
			'numeric_code' => '008',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Lempira',
			'alpha_3_code' => 'HNL',
			'numeric_code' => '340',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Mvdol',
			'alpha_3_code' => 'BOV',
			'numeric_code' => '984',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Nakfa',
			'alpha_3_code' => 'ERN',
			'numeric_code' => '232',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Netherlands Antillean Guilder',
			'alpha_3_code' => 'ANG',
			'numeric_code' => '532',
			'unicode' => 'U+0192',
			'hex_code' => '&#x192;',
			'html_entity' => '&fnof;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'New Israeli Sheqel',
			'alpha_3_code' => 'ILS',
			'numeric_code' => '376',
			'unicode' => 'U+020AA',
			'hex_code' => '&#x20AA;',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'New Zealand Dollar',
			'alpha_3_code' => 'NZD',
			'numeric_code' => '554',
			'unicode' => 'U+00024',
			'hex_code' => '&#x24;',
			'html_entity' => '&dollar;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Ngultrum',
			'alpha_3_code' => 'BTN',
			'numeric_code' => '064',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Norwegian Krone',
			'alpha_3_code' => 'NOK',
			'numeric_code' => '578',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Peso Convertible',
			'alpha_3_code' => 'CUC',
			'numeric_code' => '931',
			'unicode' => 'U+020B1',
			'hex_code' => '&#x20B1',
			'html_entity' => '&#8369;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Pound Sterling',
			'alpha_3_code' => 'GBP',
			'numeric_code' => '826',
			'unicode' => 'U+000A3',
			'hex_code' => '&#xa3;',
			'html_entity' => '&pound;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Pula',
			'alpha_3_code' => 'BWP',
			'numeric_code' => '072',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Quetzal',
			'alpha_3_code' => 'GTQ',
			'numeric_code' => '320',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Riel',
			'alpha_3_code' => 'KHR',
			'numeric_code' => '116',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Rupiah',
			'alpha_3_code' => 'IDR',
			'numeric_code' => '360',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Taka',
			'alpha_3_code' => 'BDT',
			'numeric_code' => '050',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Unidad de Fomento',
			'alpha_3_code' => 'CLF',
			'numeric_code' => '990',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 4,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Unidad de Valor Real',
			'alpha_3_code' => 'COU',
			'numeric_code' => '970',
			'unicode' => '',
			'hex_code' => '',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'US Dollar',
			'alpha_3_code' => 'USD',
			'numeric_code' => '840',
			'unicode' => 'U+00024',
			'hex_code' => '&#x24;',
			'html_entity' => '&dollar;',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Yen',
			'alpha_3_code' => 'JPY',
			'numeric_code' => '392',
			'unicode' => 'U+000A5',
			'hex_code' => '&#xa5;',
			'html_entity' => '&yen;',
			'minor_units' => 0,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('currencies')->insert([
			'short_name_en' => 'Yuan Renminbi',
			'alpha_3_code' => 'CNY',
			'numeric_code' => '156',
			'unicode' => 'U+05143',
			'hex_code' => '&#x5143;',
			'html_entity' => '',
			'minor_units' => 2,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
//
//		DB::table('currencies')->insert([
//			'short_name_en' => '',
//			'alpha_3_code' => '',
//			'numeric_code' => '',
//			'unicode' => '',
//			'hex_code' => '',
//			'html_entity' => '',
//			'minor_units' => 2,
//			'created_at' => Carbon::now(),
//			'updated_at' => Carbon::now(),
//		]);
	}
}