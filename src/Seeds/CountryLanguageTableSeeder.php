<?php

namespace ITPassionLtd\Laravel\GeoInfo\Seeds;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use ITPassionLtd\Laravel\GeoInfo\Model\Country;
use ITPassionLtd\Laravel\GeoInfo\Model\Language;

class CountryLanguageTableSeeder extends Seeder
{
	/**
	 * Run the seeder
	 *
	 * @return void
	 */
	public function run()
	{
		$albanian = Language::where('alpha_3_code', 'sqi')->first();
		$amharic = Language::where('alpha_3_code', 'amh')->first();
		$arabic = Language::where('alpha_3_code', 'ara')->first();
		$armenian = Language::where('alpha_3_code', 'hye')->first();
		$azerbaijani = Language::where('alpha_3_code', 'aze')->first();
		$belarusian = Language::where('alpha_3_code', 'bel')->first();
		$bengali = Language::where('alpha_3_code', 'ben')->first();
		$bosnian = Language::where('alpha_3_code', 'bos')->first();
		$bulgarian = Language::where('alpha_3_code', 'bul')->first();
		$catalan = Language::where('alpha_3_code', 'cat')->first();
		$chinese = Language::where('alpha_3_code', 'zho')->first();
		$croatian = Language::where('alpha_3_code', 'hrv')->first();
		$czech = Language::where('alpha_3_code', 'ces')->first();
		$danish = Language::where('alpha_3_code', 'dan')->first();
		$dutch = Language::where('alpha_3_code', 'dut')->first();
		$dzongkha = Language::where('alpha_3_code', 'dzo')->first();
		$english = Language::where('alpha_3_code', 'eng')->first();
		$estonian = Language::where('alpha_3_code', 'est')->first();
		$faroese = Language::where('alpha_3_code', 'fao')->first();
		$french = Language::where('alpha_3_code', 'fra')->first();
		$fijian = Language::where('alpha_3_code', 'fij')->first();
		$finnish = Language::where('alpha_3_code', 'fin')->first();
		$german = Language::where('alpha_3_code', 'deu')->first();
		$georgian = Language::where('alpha_3_code', 'kat')->first();
		$haitian = Language::where('alpha_3_code', 'hat')->first();
		$hebrew = Language::where('alpha_3_code', 'heb')->first();
		$hindi = Language::where('alpha_3_code', 'hin')->first();
		$hungarian = Language::where('alpha_3_code', 'hun')->first();
		$icelandic = Language::where('alpha_3_code', 'isl')->first();
		$indonesian = Language::where('alpha_3_code', 'ind')->first();
		$italian = Language::where('alpha_3_code', 'ita')->first();
		$japanese = Language::where('alpha_3_code', 'jpn')->first();
		$kalaallisut = Language::where('alpha_3_code', 'kal')->first();
		$khmer = Language::where('alpha_3_code', 'khm')->first();
		$kurdish = Language::where('alpha_3_code', 'kur')->first();
		$latin = Language::where('alpha_3_code', 'lat')->first();
		$malay = Language::where('alpha_3_code', 'msa')->first();
		$modern_greek = Language::where('alpha_3_code', 'ell')->first();
		$norwegian_nynorsk = Language::where('alpha_3_code', 'nno')->first();
		$norwegian_bokmal = Language::where('alpha_3_code', 'nob')->first();
		$papiamento = Language::where('alpha_3_code', 'pap')->first();
		$pashto = Language::where('alpha_3_code', 'pus')->first();
		$persian = Language::where('alpha_3_code', 'fas')->first();
		$portuguese = Language::where('alpha_3_code', 'por')->first();
		$rundi = Language::where('alpha_3_code', 'run')->first();
		$russian = Language::where('alpha_3_code', 'rus')->first();
		$sango = Language::where('alpha_3_code', 'sag')->first();
		$serbian = Language::where('alpha_3_code', 'srp')->first();
		$spanish = Language::where('alpha_3_code', 'spa')->first();
		$swedish = Language::where('alpha_3_code', 'swe')->first();
		$tigrinya = Language::where('alpha_3_code', 'tir')->first();
		$turkish = Language::where('alpha_3_code', 'tur')->first();

		$country = Country::where('alpha_2_code', 'AD')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $catalan->id,
		]);

		$country = Country::where('alpha_2_code', 'AF')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $persian->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $pashto->id,
		]);

		$country = Country::where('alpha_2_code', 'AG')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'AI')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'AL')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $albanian->id,
		]);

		$country = Country::where('alpha_2_code', 'AM')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $armenian->id,
		]);

		$country = Country::where('alpha_2_code', 'AO')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $portuguese->id,
		]);

		$country = Country::where('alpha_2_code', 'AR')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $spanish->id,
		]);

		$country = Country::where('alpha_2_code', 'AS')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'AT')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $german->id,
		]);

		$country = Country::where('alpha_2_code', 'AU')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'AW')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $dutch->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $papiamento->id,
		]);

		$country = Country::where('alpha_2_code', 'AX')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $swedish->id,
		]);

		$country = Country::where('alpha_2_code', 'AZ')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $azerbaijani->id,
		]);

		$country = Country::where('alpha_2_code', 'BA')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $bosnian->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $croatian->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $serbian->id,
		]);

		$country = Country::where('alpha_2_code', 'BB')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'BD')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $bengali->id,
		]);

		$country = Country::where('alpha_2_code', 'BE')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $dutch->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $french->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $german->id,
		]);

		$country = Country::where('alpha_2_code', 'BF')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $french->id,
		]);

		$country = Country::where('alpha_2_code', 'BG')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $bulgarian->id,
		]);

		$country = Country::where('alpha_2_code', 'BH')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $arabic->id,
		]);

		$country = Country::where('alpha_2_code', 'BI')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $french->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $rundi->id,
		]);

		$country = Country::where('alpha_2_code', 'BJ')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $french->id,
		]);

		$country = Country::where('alpha_2_code', 'BM')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'BN')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $malay->id,
		]);

		$country = Country::where('alpha_2_code', 'BO')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $spanish->id,
		]);

		$country = Country::where('alpha_2_code', 'BQ')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $dutch->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $papiamento->id,
		]);

		$country = Country::where('alpha_2_code', 'BR')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $portuguese->id,
		]);

		$country = Country::where('alpha_2_code', 'BS')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'BT')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $dzongkha->id,
		]);

		$country = Country::where('alpha_2_code', 'BV')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $norwegian_nynorsk->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $norwegian_bokmal->id,
		]);

		$country = Country::where('alpha_2_code', 'BW')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'BY')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $belarusian->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $russian->id,
		]);

		$country = Country::where('alpha_2_code', 'BZ')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'CA')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $french->id,
		]);

		$country = Country::where('alpha_2_code', 'CC')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'CD')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $french->id,
		]);

		$country = Country::where('alpha_2_code', 'CF')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $french->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $sango->id,
		]);

		$country = Country::where('alpha_2_code', 'CG')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $french->id,
		]);

		$country = Country::where('alpha_2_code', 'CI')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $french->id,
		]);

		$country = Country::where('alpha_2_code', 'CK')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'CL')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $spanish->id,
		]);

		$country = Country::where('alpha_2_code', 'CM')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $french->id,
		]);

		$country = Country::where('alpha_2_code', 'CN')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $chinese->id,
		]);

		$country = Country::where('alpha_2_code', 'CO')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $spanish->id,
		]);

		$country = Country::where('alpha_2_code', 'CR')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $spanish->id,
		]);

		$country = Country::where('alpha_2_code', 'CU')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $spanish->id,
		]);

		$country = Country::where('alpha_2_code', 'CV')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $portuguese->id,
		]);

		$country = Country::where('alpha_2_code', 'CW')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $dutch->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $papiamento->id,
		]);

		$country = Country::where('alpha_2_code', 'CX')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'CY')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $modern_greek->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $turkish->id,
		]);

		$country = Country::where('alpha_2_code', 'CZ')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $czech->id,
		]);

		$country = Country::where('alpha_2_code', 'DE')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $german->id,
		]);

		$country = Country::where('alpha_2_code', 'DJ')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $arabic->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $french->id,
		]);

		$country = Country::where('alpha_2_code', 'DK')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $danish->id,
		]);

		$country = Country::where('alpha_2_code', 'DM')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'DO')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $spanish->id,
		]);

		$country = Country::where('alpha_2_code', 'DZ')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $arabic->id,
		]);

		$country = Country::where('alpha_2_code', 'EC')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $spanish->id,
		]);

		$country = Country::where('alpha_2_code', 'EE')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $estonian->id,
		]);

		$country = Country::where('alpha_2_code', 'EG')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $arabic->id,
		]);

		$country = Country::where('alpha_2_code', 'ER')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $tigrinya->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $arabic->id,
		]);

		$country = Country::where('alpha_2_code', 'ET')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $amharic->id,
		]);

		$country = Country::where('alpha_2_code', 'FI')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $finnish->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $swedish->id,
		]);

		$country = Country::where('alpha_2_code', 'FJ')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $fijian->id,
		]);

		$country = Country::where('alpha_2_code', 'FK')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'FO')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $danish->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $faroese->id,
		]);

		$country = Country::where('alpha_2_code', 'FR')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $french->id,
		]);

		$country = Country::where('alpha_2_code', 'GA')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $french->id,
		]);

		$country = Country::where('alpha_2_code', 'GD')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'GE')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $georgian->id,
		]);

		$country = Country::where('alpha_2_code', 'GF')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $french->id,
		]);

		$country = Country::where('alpha_2_code', 'GG')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $french->id,
		]);

		$country = Country::where('alpha_2_code', 'GH')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'GI')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'GL')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $danish->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $kalaallisut->id,
		]);

		$country = Country::where('alpha_2_code', 'GM')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'GN')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $french->id,
		]);

		$country = Country::where('alpha_2_code', 'GP')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $french->id,
		]);

		$country = Country::where('alpha_2_code', 'GQ')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $spanish->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $french->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $portuguese->id,
		]);

		$country = Country::where('alpha_2_code', 'GR')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $modern_greek->id,
		]);

		$country = Country::where('alpha_2_code', 'GT')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $spanish->id,
		]);

		$country = Country::where('alpha_2_code', 'GU')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'GW')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $portuguese->id,
		]);

		$country = Country::where('alpha_2_code', 'GY')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'HK')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $chinese->id,
		]);

		$country = Country::where('alpha_2_code', 'HM')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'HN')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $spanish->id,
		]);

		$country = Country::where('alpha_2_code', 'HR')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $croatian->id,
		]);

		$country = Country::where('alpha_2_code', 'HT')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $french->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $haitian->id,
		]);

		$country = Country::where('alpha_2_code', 'HU')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $hungarian->id,
		]);

		$country = Country::where('alpha_2_code', 'ID')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $indonesian->id,
		]);

		$country = Country::where('alpha_2_code', 'IE')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'IL')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $arabic->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $hebrew->id,
		]);

		$country = Country::where('alpha_2_code', 'IM')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'IN')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $hindi->id,
		]);

		$country = Country::where('alpha_2_code', 'IO')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'IQ')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $arabic->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $kurdish->id,
		]);

		$country = Country::where('alpha_2_code', 'IR')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $persian->id,
		]);

		$country = Country::where('alpha_2_code', 'IS')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $icelandic->id,
		]);

		$country = Country::where('alpha_2_code', 'IT')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $italian->id,
		]);

		$country = Country::where('alpha_2_code', 'JE')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $french->id,
		]);

		$country = Country::where('alpha_2_code', 'JM')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'JO')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $arabic->id,
		]);

		$country = Country::where('alpha_2_code', 'JP')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $japanese->id,
		]);

		$country = Country::where('alpha_2_code', 'KH')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $khmer->id,
		]);

		$country = Country::where('alpha_2_code', 'KM')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $arabic->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $french->id,
		]);

		$country = Country::where('alpha_2_code', 'KY')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $english->id,
		]);

		$country = Country::where('alpha_2_code', 'PF')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $french->id,
		]);

		$country = Country::where('alpha_2_code', 'SV')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $spanish->id,
		]);

		$country = Country::where('alpha_2_code', 'TD')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $arabic->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $french->id,
		]);

		$country = Country::where('alpha_2_code', 'TF')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $french->id,
		]);

		$country = Country::where('alpha_2_code', 'VA')->first();
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $italian->id,
		]);
		DB::table('country_language')->insert([
			'country_id' => $country->id,
			'language_id' => $latin->id,
		]);
	}
}
