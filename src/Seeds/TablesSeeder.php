<?php

namespace ITPassionLtd\Laravel\GeoInfo\Seeds;

use Illuminate\Database\Seeder;

class TablesSeeder extends Seeder
{
	/**
	 * Run the seeder
	 *
	 * @return void
	 */
	public function run()
	{
		$this->call(CountriesTableSeeder::class);
		$this->call(LanguagesTableSeeder::class);
		$this->call(CountryLanguageTableSeeder::class);
		$this->call(CurrenciesTableSeeder::class);
		$this->call(CountryCurrencyTableSeeder::class);
	}
}