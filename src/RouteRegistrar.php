<?php

namespace ITPassionLtd\Laravel\GeoInfo;

use Illuminate\Contracts\Routing\Registrar as Router;

class RouteRegistrar
{
	/**
	 * The router implementation
	 *
	 * @var Router
	 */
	protected $router;

	/**
	 * RouteRegistrar constructor.
	 *
	 * @param Router $router
	 *
	 * @return void
	 */
	public function __construct(Router $router)
	{
		$this->router = $router;
	}

	/**
	 * Register all the routes
	 *
	 * @return void
	 */
	public function all()
	{
		$this->for_countries();
		$this->for_locales();
	}

	/**
	 * Register the routes for the countries
	 *
	 * @return void
	 */
	protected function for_countries()
	{
		$this->router->group([
			'middleware' => ['api']
		], function ($router) {
			$router->get('/countries', 'CountriesController@index')
				->name('geoinfo-countries.index');
			$router->get('/countries/{country}', 'CountriesController@show')
				->name('geoinfo-countries.show');
			$router->get('/country_from_client_ip',
					'CountriesController@from_client_ip')
				->name('geoinfo-countries.from_client_ip');
		});
	}

	/**
	 * Register the routes for the locales
	 *
	 * @return void
	 */
	protected function for_locales()
	{
		$this->router->group([
			'middleware' => ['api']
		], function ($router) {
			$router->get('/locale_from_client_ip',
					'LocalesController@from_client_ip')
				->name('geoinfo-locales.from_client_ip');
		});
	}
}