<?php

namespace ITPassionLtd\Laravel\GeoInfo\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use ITPassionLtd\Laravel\GeoInfo\Model\Country;

class CountriesController extends BaseController
{
	/**
	 * Retrieve the locale, based on the Client's IP address and then finding
	 * the default language in that country
	 *
	 * @return mixed
	 */
	public function from_client_ip()
	{
		Log::debug('>>> ' . __METHOD__);

		/* TODO Implement ipinfo lookup */

//		if(Request::ip() == '127.0.0.1') {
//			return response('', 404);
//		}

		$country = Country::where('alpha_2_code', 'IE')->get();
		$result = $country->first()->toJson();

		ob_start();
		var_dump($result);
		Log::debug('    Returning "' . ob_get_contents() . '"');
		ob_end_clean();

		Log::debug('<<< ' . __METHOD__);
		return response($result, 200);
	}

	/**
	 * Show all countries
	 *
	 * @return mixed
	 */
	public function index()
	{
		$countries = Country::all()->sortBy('en_short_name');

		if($countries->count()) {
			return response()->json($countries);
		} else {
			return response(json_encode([
				'error' => 'No Country entities exist']), 404);
		}
	}

	/**
	 * Show the provided country as JSON entity
	 *
	 * @param Country $country
	 *
	 * @return string
	 */
	public function show(Country $country)
	{
		return $country->toJson();
	}
}