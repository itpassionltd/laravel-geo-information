<?php
/**
 * Created by PhpStorm.
 * User: leeuwg
 * Date: 01/09/17
 * Time: 10:13
 */

namespace ITPassionLtd\Laravel\GeoInfo\Http\Controllers;

use Illuminate\Support\Facades\Request;

class LocalesController extends BaseController
{
	/**
	 * Retrieve the locale, based on the Client's IP address and then finding
	 * the default language in that country
	 *
	 * @return mixed
	 */
	public function from_client_ip()
	{
		/* TODO Implement ipinfo lookup */

//		if(Request::ip() == '127.0.0.1') {
//			return response('', 404);
//		}

		return response()->json(['locale' => 'en-ie']);
	}
}