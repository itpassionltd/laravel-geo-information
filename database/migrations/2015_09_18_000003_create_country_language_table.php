<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateCountryLanguageTable extends Migration
{
	/**
	 * Run the migrations
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('country_language', function(Blueprint $table) {
			$table->integer('country_id')->unsigned();
			$table->integer('language_id')->unsigned();
			$table->primary(['country_id', 'language_id']);

			$table->foreign('country_id')->references('id')->on('countries')
				->onDelete('restrict')->onUpdate('restrict');
			$table->foreign('language_id')->references('id')->on('languages')
				->onDelete('restrict')->onUpdate('restrict');
		});
	}

	/**
	 * Reverse the migrations
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('country_language');
	}
}