<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateCurrenciesTable extends Migration
{
	/**
	 * Run the migrations
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('currencies', function(Blueprint $table) {
			$table->increments('id');
			$table->string('short_name_en', 191);
			$table->char('alpha_3_code', 3);
			$table->char('numeric_code', 3);
			$table->char('unicode', 7)->nullable();
			$table->char('hex_code', 8)->nullable();
			$table->string('html_entity', 25)->nullable();
			$table->smallInteger('minor_units')->unsigned();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('currencies');
	}
}
