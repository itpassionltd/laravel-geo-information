<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
	/**
	 * Run the migrations
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('countries', function(Blueprint $table) {
			$table->increments('id');
			$table->char('alpha_2_code', 2)->unique();
			$table->char('alpha_3_code', 3)->unique();
			$table->char('numeric_code', 3)->unique();
			$table->boolean('independent')->default(1);
			$table->string('status')->default('officially-assigned');
			$table->string('short_name_en')->index();
			$table->string('short_name_uppercase_en')->index();
			$table->string('full_name_en')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('countries');
	}
}