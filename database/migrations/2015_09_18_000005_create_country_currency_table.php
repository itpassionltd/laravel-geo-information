<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateCountryCurrencyTable extends Migration
{
	/**
	 * Run the migrations
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('country_currency', function(Blueprint $table) {
			$table->integer('country_id')->unsigned();
			$table->integer('currency_id')->unsigned();
			$table->primary(['country_id', 'currency_id']);

			$table->foreign('country_id')->references('id')->on('countries')
				->onDelete('restrict')->onUpdate('restrict');
			$table->foreign('currency_id')->references('id')->on('currencies')
				->onDelete('restrict')->onUpdate('restrict');
		});
	}

	/**
	 * Reverse the migrations
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('country_currency');
	}
}