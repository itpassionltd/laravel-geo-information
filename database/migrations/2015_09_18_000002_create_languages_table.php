<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateLanguagesTable extends Migration
{
	/**
	 * Run the migrations
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('languages', function(Blueprint $table) {
			$table->increments('id');
			$table->char('alpha_3_code', 3)->unique();
			$table->char('alpha_3_code_synonym', 3)->nullable();
			$table->char('alpha_2_code', 2)->nullable();
			$table->string('short_name_en')->index();
			$table->string('short_name_fr')->index();
			$table->string('short_name_de')->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('languages');
	}
}