# itpassionltd/laravel-geo-information

## Installation

In order to install please follow the below steps:
1. `composer require itpassionltd/laravel-geo-information`

   This will install the package in your vendor directory.

   If you have laravel >= 5.5 you can skip ahead to step 3.

2. Add `ITPassionLtd\Laravel\GeoInfo\GeoInfoServiceProvider::class` to the providers array in `config/app.php`

   This will give the package an opportunity to register itself with Laravel

3. Run `php artisan migrate` to install the database tables

4. Run `php artisan db:seed --class=ITPassionLtd\\Laravel\\GeoInfo\\Seeds\\GeoInfoTablesSeeders` to seed the created tables

That's it: The installation is finished now

## Usage

Simply use `ITPassionLtd\\Laravel\\GeoInfo\\App\\Country` to use the Country Model in your code.

## TODO

Seeding:
* Finsih the GeoInfoCountriesTableSeeder
* Create the GeoInfoTablesSeeder

Create the following APIs:
* GET countries
* GET countries/{country}

Create tables and seeders for
* Regions
* Languages
* Currency Symbols
